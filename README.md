# ResourceServer

Resource server to access resource by using access token.

Endpoints : 

1. GET - localhost:8080/services/products
2. POST - localhost:8080/services/products
3. GET - localhost:8080/services/{productId}