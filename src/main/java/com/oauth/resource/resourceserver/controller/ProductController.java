package com.oauth.resource.resourceserver.controller;

import com.oauth.resource.resourceserver.model.Product;
import com.oauth.resource.resourceserver.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/services")
public class ProductController {


    @Autowired
    private ProductService productService;

    @Autowired
    private TokenStore tokenStore;

    @PreAuthorize("hasAnyRole('ROLE_admin')")
    @PostMapping("/products")
    public Product save(@RequestBody Product product) {
        return productService.save(product);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_user')")
    @GetMapping("/products/{productId}")
    public Product fetch(@PathVariable int productId,OAuth2Authentication authentication) {
        return productService.fetchById(productId);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_user')")
    @GetMapping("/products")
    public List<Product> fetch(OAuth2Authentication authentication) {

        OAuth2AuthenticationDetails auth2AuthenticationDetails = (OAuth2AuthenticationDetails) authentication.getDetails();
        Map<String, Object> details = tokenStore.readAccessToken(auth2AuthenticationDetails.getTokenValue()).getAdditionalInformation();
        //These variable are initialized just to check can we access additional information from the accessToken.
        Integer userId = (Integer) details.get("userId");
        String userName = (String) details.get("userName");
        String email = (String) details.get("Email");
        System.out.println(userId+" "+userName+" "+email);

        return productService.fetchAllProfiles();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/revoke")
    @ResponseStatus(HttpStatus.OK)
    public void revokeToken(OAuth2Authentication authentication) {
        OAuth2AuthenticationDetails auth2AuthenticationDetails = (OAuth2AuthenticationDetails) authentication.getDetails();
        tokenStore.removeAccessToken(tokenStore.readAccessToken(auth2AuthenticationDetails.getTokenValue()));
    }
}
