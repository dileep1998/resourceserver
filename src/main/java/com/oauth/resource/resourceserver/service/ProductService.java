package com.oauth.resource.resourceserver.service;

import com.oauth.resource.resourceserver.model.Product;

import java.util.List;

public interface ProductService {


    Product save(Product product);

    Product fetchById(int profileId);

    List<Product> fetchAllProfiles();
}
