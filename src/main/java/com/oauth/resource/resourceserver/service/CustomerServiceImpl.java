package com.oauth.resource.resourceserver.service;

import com.oauth.resource.resourceserver.model.Product;
import com.oauth.resource.resourceserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product fetchById(int productId) {
        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()) {
            return product.get();
        } else {
            return null;
        }
    }

    @Override
    public List<Product> fetchAllProfiles() {
        return productRepository.findAll();
    }
}
